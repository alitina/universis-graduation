const package = require('./package.json');
const path = require('path');
const rimraf = require('rimraf');
const copy = require('copy-concurrently');
// get source directory ./assets
const src = path.resolve(__dirname, 'src/assets');
// get destination directory ../../dist/forms/assets
// check if package name uses scope
let packageName =package.name;
if (/^@/g.test(packageName)) {
    // get package name without scope
    packageName = package.name.split('/')[1];
}
dest = path.resolve(__dirname, `dist/graduation/assets`);
// remove destination files
rimraf.sync(dest);
console.log('Copy Assets');
console.log('From: ' + src);
console.log('To: ' + dest);
// copy assets
copy(src, dest).then(() => {
    // copy assets completed
    return process.exit(0);
}).catch(err => {
    console.log('ERROR', err);
    // exit with error
    return process.exit(-2);
});
