import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgPipesModule } from 'ngx-pipes';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { GraduationRoutingModule } from './graduation.router';
import { GraduationRequestWizardDirective } from './directives/graduation-request-step-directive';
import { HttpClient } from '@angular/common/http';
import { GraduationRootComponent } from './components/graduation-root/graduation-root.component';
import {GraduationRulesComponent} from './components/graduation-root/graduation-rules/graduation-rules.component';
import {GraduationRequestComponent} from './components/graduation-root/graduation-apply/graduation-request/graduation-request.component';
import {GraduationRequestFormComponent} from './components/graduation-root/graduation-apply/graduation-request/steps/graduation-request-form/graduation-request-form.component';
import {GraduationRequirementsCheckComponent} from './components/graduation-root/graduation-apply/graduation-request/steps/graduation-requirements-check/graduation-requirements-check.component';
import {GraduationDocumentSubmissionComponent} from './components/graduation-root/graduation-apply/graduation-request/steps/graduation-document-submission/graduation-document-submission.component';
import {GraduationCeremonyComponent} from './components/graduation-root/graduation-apply/graduation-request/steps/graduation-ceremony/graduation-ceremony.component';
import {GraduationWizardTabComponent} from './components/graduation-root/graduation-apply/graduation-wizard-tab/graduation-wizard-tab.component';
import {GraduationApplyComponent} from './components/graduation-root/graduation-apply/graduation-apply.component';
import {GraduationPrerequisitesContainerComponent} from './components/graduation-root/graduation-prerequisites-container.component';
import {GraduationSharedModule} from './graduation-shared.module';
import {GraduationProgressContainerComponent} from './components/graduation-root/graduation-progress-container.component';
import { GraduationTreeComponent } from './components/graduation-root/graduation-rules/graduation-tree/graduation-tree.component';
import {SharedModule} from '@universis/common';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    GraduationRoutingModule,
    NgPipesModule,
    SharedModule,
    GraduationSharedModule.forRoot()
  ],
  declarations: [
    GraduationRequestComponent,
    GraduationRequestFormComponent,
    GraduationRequirementsCheckComponent,
    GraduationDocumentSubmissionComponent,
    GraduationCeremonyComponent,
    GraduationApplyComponent,
    GraduationWizardTabComponent,
    GraduationRequestWizardDirective,
    GraduationRootComponent,
    GraduationRulesComponent,
    GraduationPrerequisitesContainerComponent,
    GraduationProgressContainerComponent
  ],
  entryComponents: [
    GraduationRequestFormComponent,
    GraduationRequirementsCheckComponent,
    GraduationDocumentSubmissionComponent,
    GraduationCeremonyComponent
  ]
})
export class GraduationModule {
  constructor() {

  }
}
