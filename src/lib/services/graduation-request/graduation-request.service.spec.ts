import { TestBed } from '@angular/core/testing';
import { GraduationService } from './graduation.service';

describe('GraduationRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ ]
    })
    .compileComponents();
  });

  it('should be created', () => {
    const service: GraduationService = TestBed.get(GraduationService);
    expect(service).toBeTruthy();
  });
});
