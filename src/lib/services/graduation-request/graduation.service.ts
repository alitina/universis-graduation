import {Injectable} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';
import {asyncMemoize, ConfigurationService, DiagnosticsService} from '@universis/common';
import {GraduationRequestStep} from '../../graduation-request-step';
import {
  GraduationRequestFormComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-request-form/graduation-request-form.component';
import {
  GraduationRequirementsCheckComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-document-submission/graduation-document-submission.component';
import {
  GraduationCeremonyComponent
} from '../../components/graduation-root/graduation-apply/graduation-request/steps/graduation-ceremony/graduation-ceremony.component';

/**
 *
 * Handles data between api and the students app regarding the graduation process
 *
 */

@Injectable({
  providedIn: 'root'
})
export class GraduationService {

  constructor(
    private _context: AngularDataContext,
    private _configuration: ConfigurationService,
    private _httpClient: HttpClient,
    private _diagnostics: DiagnosticsService
  ) {
  }

  async getStudentRequestConfigurations() {
    // this._configuration.currentLocale
    return this._context.model('StudentRequestConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal('el')
      .expand('category')
      .getItems();
  }

/*  /!**
   *
   * Fetches the available event status
   *
   *!/
  async getAvailableGraduationEvents() {
    return this._context.model('students/me/availableGraduationEvents')
      .asQueryable()
      .expand('graduationYear,graduationPeriod,location')
      .take(-1)
      .getItems();
  }*/

 /* /!**
   *
   * Get the student's graduation request
   *
   *!/
  async getGraduationRequest(student: any) {
    const graduationRequests = await this._context.model(`students/${student}/graduationRequests`)
      .asQueryable()
      // .and('graduationEvent/validThrough').lowerOrEqual(currentDate)
      .where
      ('actionStatus/alternateName').notEqual('CancelledActionStatus').and
      ('actionStatus/alternateName').notEqual('FailedActionStatus')
      // tslint:disable-next-line:max-line-length
      .expand('graduationEvent($expand=graduationPeriod,graduationYear,location,attachmentTypes($expand=attachmentType),eventStatus),attachments($expand=attachmentType),student($expand=studentStatus)')
      .orderByDescending('dateCreated')
      .take(-1)
      .getItems();
    const currentRequest = graduationRequests && graduationRequests.length > 0 ? graduationRequests[0] : null;
    return currentRequest;
  }*/

  /**
   *
   * Get the student's graduation request
   *
   */
  async checkAccessGraduationRequest(student: any) {
    const result = this._context.model(`students/${student}/checkAccessGraduationRequest`)
      .asQueryable()
      .getItem();
    return result;
  }

  /**
   *
   * Gets the graduation rules for a student
   *
   */
  getGraduationRules(student: any): any {
    return this._context.model(`students/${student}/graduationRules`)
      .asQueryable()
      .expand('validationResult')
      .take(-1)
      .getItems();
  }

  /**
   *
   * Creates a graduation request for a student
   *
   * @param graduationEvent The graduation event for which the student is applying
   * @param name The special request from the student
   *
   */
  async setGraduationRequest(graduationEventId: number, graduationForm) {
      const graduationEvent = await this._context.model(`graduationRequestActions`).save({
        graduationEvent: {
          id: graduationEventId
        },
        name: graduationForm.name,
        description: graduationForm.specialRequest
      });
      return graduationEvent;
  }

  /**
   *
   * Gets information regarding the course types
   *
   */
  @asyncMemoize()
  getCourseTypes(): any {
    return this._context.model('courseTypes')
      .take(-1)
      .getItems();
  }

  /**
   *
   * Gets information regarding the student
   *
   */
  async getStudent(student: any) {
    return this._context.model(`students/${student}`)
      .asQueryable()
      .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'person($expand=gender)')
      .getItem().then(result => {
        if (typeof result === 'undefined') {
          return Promise.reject('Student data cannot be found');
        }
        return this._context.model('LocalDepartments')
          .asQueryable()
          .where('id').equal(result.department.id)
          .expand('organization($expand=instituteConfiguration,registrationPeriods)')
          .getItem().then(department => {
            if (typeof department === 'undefined') {
              return Promise.reject('Student department cannot be found');
            }
            result.department = department;
            sessionStorage.setItem('student', JSON.stringify(result));
            return Promise.resolve(result);
          });
      });
  }

  /**
   *
   * Creates a graduation request for a student
   *
   * @param graduationEvent The graduation event for which the student is applying
   * @param name The special request from the student
   *
   */
  async setGraduationRequestActiveStatus(data) {
    try {
      await this._context.model(`graduationRequestActions`).save({
        additionalType: 'GraduationRequestAction',
        graduationEvent: {
          id: data.data.request.graduationEvent.id
        },
        actionStatus: {
          additionalType: 'ActionStatusType',
          alternateName: 'ActiveActionStatus',
          name: 'Active',
          id: 4
        },
        student: data.data.request.student.id,
        owner: data.data.request.owner,
        id: data.data.request.id
      });
    } catch (err) {
      console.log(err);
      return null;
    }
  }


  /**
   * Uploads a graduation attachment
   */
  async uploadGraduationRequestAttachment(data) {
    const formData: FormData = new FormData();
    formData.append('file', data.file, data.file.name);
    formData.append('file[attachmentType]', data.attachmentType);

    // get context service headers
    const headers = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`students/me/requests/${data.graduationRequestId}/attachments/add`);

    await this._httpClient.post(postUrl, formData, {
      headers: headers
    }).toPromise().then(result => {
      return result;
    });
  }

  /**
   *
   * Remove a graduation request document
   *
   * @param requestId The id of the request
   * @param attachmentId  The id of the attachment
   *
   */
  async removeGraduationRequestAttachment(data) {
    const requestId = data.requestId;
    const attachmentId = data.attachmentId;
    const attachmentTypeAlternateName = data.attachmentTypeAlternateName;
    try {
      await this._context.model(`students/me/requests/${requestId}/attachments/${attachmentId}/remove`).save({});
    } catch (err) {
      return  err;
    }
  }

  /**
   *
   * Downloads the a graduation request attachment
   *
   * @param data The data that contains information about the file
   *
   */
  async downloadGraduationRequestAttachment(data): Promise<any> {
    const attachmentAlternateName = data.attachmentAlternateName;
    const attachmentTypeAlternateName = data.attachmentTypeAlternateName;
    try {
      const headers = this._context.getService().getHeaders();
      const base = this._context.getBase();
      const file = await this._httpClient.get(`${base}content/private/${attachmentAlternateName}`, {
        responseType: 'blob',
        headers
      }).toPromise();

      this.triggerFileDownloadPopup(file, attachmentAlternateName);
    } catch (err) {
      return err;
    }
  }

  /**
   *
   * triggers a file download popup at the browser
   *
   * @param blob The file as received from the server
   */
  triggerFileDownloadPopup(blob: Blob, fileName: string) {
    const objectUrl = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = objectUrl;
    const extension = 'txt'; // TODO: change the extension
    a.download = `${fileName}-.${extension}`;
    a.click();
    window.URL.revokeObjectURL(objectUrl);
    a.remove();
  }

  /**
   *
   * Resolves alternateNames to angular components
   *
   * @param alternateName The alternate name of the component
   *
   */
  getComponent(alternateName: string): GraduationRequestStep {
    switch (alternateName) {
      case 'graduationRequest':
        return new GraduationRequestStep(GraduationRequestFormComponent, {});
      case 'graduationRequirementsCheck':
        return new GraduationRequestStep(GraduationRequirementsCheckComponent, {});
      case 'graduationDocumentsSubmission':
        return new GraduationRequestStep(GraduationDocumentSubmissionComponent, {});
      case 'graduationCeremony':
        return new GraduationRequestStep(GraduationCeremonyComponent, {});
    }
  }

  async getStepData(stepData, graduationRequest, graduationEvents, student) {
    if (stepData.index === 0) {
      /**
       * 1st step
       * The graduation request is available only if there is not
       * a not failed or canceled request
       */
      const graduationRequestStatus = graduationRequest ? 'completed' : 'failed';
      const graduationUserConsents = await this.getGraduationUserConsents();

      const data = {
        request: graduationRequest,
        events: graduationEvents,
        userConsents: graduationUserConsents
      };
      stepData.status = graduationRequestStatus;
      stepData.data = data;

    } else if (stepData.index === 1) {
      /**
       * 2st step
       * The graduationRequirementsCheck step is available only if there is a graduation request.
       * It is unavailable if there is not a graduation request.
       * It is failed if there is at least a failed step.
       */
      const graduationRules = await this.getGraduationRules('me');
      const graduationRequirementsStatus = this.getGraduationRequirementsStepStatus(!!graduationRequest, graduationRules);

      const data = {
        request: graduationRequest,
        student: student,
        graduationInfo: graduationRules
      };
      stepData.status = graduationRequirementsStatus;
      stepData.data = data;
    } else if (stepData.index === 2) {
      /**
       * 3rd step
       * The documents submission step is available only if the requirements check have passed
       * It is completed when the documents are accepted
       * It is failed if a document is rejected
       */
      const attachments = this.getDocuments(graduationRequest);
      // tslint:disable-next-line:max-line-length
      const graduationDocumentsSubmissionStatus = this.getDocumentsSubmissionStepStatus(attachments, graduationRequest.graduationEvent);
      const data = {
        request: graduationRequest
      };
      stepData.status = graduationDocumentsSubmissionStatus;
      stepData.data = data;
    } else if (stepData.index === 3) {
      /**
       * 4th step
       */
      // 1st status
      const graduationRequestStatus = graduationRequest ? 'completed' : 'failed';
      // 2st status
      const graduationRules = await this.getGraduationRules('me');
      const graduationRequirementsStatus = this.getGraduationRequirementsStepStatus(!!graduationRequest, graduationRules);
      // 3rd status
      const attachments = this.getDocuments(graduationRequest);
      // tslint:disable-next-line:max-line-length
      const graduationDocumentsSubmissionStatus = this.getDocumentsSubmissionStepStatus(attachments, graduationRequest.graduationEvent);

      let graduationCeremonyStepStatus;
      if (graduationRequestStatus === 'completed' && graduationRequirementsStatus === 'completed'
        && (graduationDocumentsSubmissionStatus === 'completed' || graduationDocumentsSubmissionStatus === 'unavailable')) {
        graduationCeremonyStepStatus = 'completed';
      } else {
        graduationCeremonyStepStatus = 'failed';
      }
      const data = {
        request: graduationRequest,
        certificates: []
      };
      stepData.status = graduationCeremonyStepStatus;
      stepData.data = data;
    }
    return stepData;
  }


  getGraduationRequirementsStepStatus(graduationRequestExists: boolean, graduationRules) {
    if (!graduationRequestExists || !graduationRules.finalResult) {
      return 'unavailable';
    } else if (!graduationRules.finalResult.success) {
      return 'failed';
    } else if (graduationRules.finalResult.success) {
      return 'completed';
    } else {
      return 'unavailable';
    }
  }

  /**
   *
   * Matches a graduation request action with a graduation request action type.
   *
   * @param requestActionTypes The list of available action types
   * @param requestActions The list of the request actions (documents)
   *
   */
  getDocuments(graduationRequest: any) {

    const graduationEvent =  graduationRequest.graduationEvent;
    let requestActionTypes = graduationEvent && graduationEvent.attachmentTypes ? graduationEvent.attachmentTypes : [];
    let requestActions = graduationRequest && graduationRequest.attachments ? graduationRequest.attachments : [];

    // exclude physical attachments from step status control
    requestActionTypes = requestActionTypes.filter((actionType) =>
      !actionType.attachmentType || !actionType.attachmentType.physical
    );

    requestActions = requestActions.filter((actionType) =>
      !actionType.physical === true
    );

    const filteredRequestActionTypes = requestActionTypes.filter((actionType) =>
      !!actionType.attachmentType && actionType.attachmentType.name && !!actionType.attachmentType.alternateName
    );
    if (filteredRequestActionTypes.length !== requestActionTypes.length) {
      console.warn('Graduation action type(s) without name or alternateName found and removed');
    }

    // check if the attachments  files are the required ones
    let requestDocumentsStatus = filteredRequestActionTypes.map((requestActionType) => {
      const selectedRequestAction = requestActions.filter(
        (requestAction) => requestAction.attachmentType.id === requestActionType.attachmentType.id
      );
      return {
        alternateName: requestActionType.attachmentType.alternateName,
        name: requestActionType.attachmentType.name,
        description: requestActionType.attachmentType.description,
        document: requestActionType,
        status: selectedRequestAction && selectedRequestAction.length > 0 ? 'completed' : 'pending',
        requestActionType
      };
    });

    requestDocumentsStatus = requestDocumentsStatus.filter(
      (requestDocument) => requestDocument && requestDocument.alternateName
    );
    return requestDocumentsStatus;
  }

  getDocumentsSubmissionStepStatus(attachments, graduationEvent: any) {
    if (!graduationEvent || !graduationEvent.attachmentTypes || graduationEvent.attachmentTypes.length === 0) {
      return 'unavailable';
    } else if (attachments && attachments.length > 0 &&
      graduationEvent && graduationEvent.attachmentTypes && graduationEvent.attachmentTypes.length > 0) {
      const results = attachments.filter(
        (requestAction) => requestAction.status === 'completed'
      );

      // exclude physical attachments from step status control
      let requestActionTypes = graduationEvent.attachmentTypes ? graduationEvent.attachmentTypes : [];
      requestActionTypes = requestActionTypes.filter((actionType) =>
        !actionType.attachmentType.physical
      );

      if (results && results.length > 0 && results.length === requestActionTypes.length) {
        return 'completed';
      }
      return 'failed';
    } else {
      return 'unavailable';
    }
  }

  async getGraduationUserConsents(): Promise<any[]> {
    let consentSupported = false;
    try {
      consentSupported = await this._diagnostics.hasService('UserConsentService');
    } catch (err) {
      console.error(err);
    }
    if (!consentSupported) {
      return [];
    }
    const consents = await this._context.model(`users/me/consents`).getItems();
    const fields = consents && consents.value && consents.value["graduatedStudent"];
    if (!fields) return [];
    delete fields["metadata"];
    // convert from object to list
    return Object.keys(fields)
      .map(key => Object.assign({
        alternateName: key,
      }, fields[key]));
  }

  async setGraduationUserConsents(fields: any[]): Promise<any> {
    // convert from list to object
    const data = {};
    fields
      .map((field) => {
        if (field.val === 'u') {
          field.val = 'n';
        }
        return field;
      })
      .forEach(i => data[i.alternateName] = i);
    return this._context.model(`users/me/consents`).save({
      "graduatedStudent": data
    });
  }

}
