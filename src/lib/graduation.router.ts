import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GraduationRootComponent} from './components/graduation-root/graduation-root.component';
import {GraduationRulesComponent} from './components/graduation-root/graduation-rules/graduation-rules.component';
import {GraduationRequestComponent} from './components/graduation-root/graduation-apply/graduation-request/graduation-request.component';
import {GraduationApplyComponent} from './components/graduation-root/graduation-apply/graduation-apply.component';
import {GraduationPrerequisitesContainerComponent} from './components/graduation-root/graduation-prerequisites-container.component';
import {GraduationProgressContainerComponent} from './components/graduation-root/graduation-progress-container.component';


export const graduationRequestRoutes: Routes = [
  {
    path: '',
    component: GraduationRootComponent,
    data: {
      title: 'Graduation'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'rules'
      },
      {
        path: 'rules',
        component: GraduationRulesComponent,
        data: {
          title: 'Graduation Prerequisites'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'prerequisites'
          },
          {
            path: 'prerequisites',
            component: GraduationPrerequisitesContainerComponent
          },
          {
            path: 'progress',
            component: GraduationProgressContainerComponent
          }
        ]
      },
      {
        path: 'apply',
        component: GraduationApplyComponent
      },
      {
        path: 'request',
        component: GraduationRequestComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(graduationRequestRoutes)],
  exports: [RouterModule]
})
export class GraduationRoutingModule {
  constructor() {}
}
