import { Component, OnInit, OnDestroy, Output, ViewChild, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { GraduationWizardTabComponent } from '../../../graduation-wizard-tab/graduation-wizard-tab.component';
import {number} from 'mathjs';
import { GraduationService } from '../../../../../../services/graduation-request/graduation.service';
import { ErrorService } from '@universis/common';

@Component({
  selector: 'universis-graduation-request-form',
  templateUrl: './graduation-request-form.component.html',
  styleUrls: ['./graduation-request-form.component.scss']
})
export class GraduationRequestFormComponent extends GraduationWizardTabComponent implements OnInit, OnDestroy {

  /**
   *
   * The graduation request special request form
   *
   */
  @ViewChild('studentGraduationRequest') studentGraduationRequest: NgForm;

  /**
   * Notifies that there is a form submission
   */
  @Output() currentStepOutput = new EventEmitter();

  /**
   *
   * States whether the student has already submitted the form
   *
   */
  public submitted: boolean;

  /**
   *
   * Dictates if a graduation request can be submitted
   *
   */
  public canSubmit = false;

  /**
   *
   * A list of items that the student can consent to
   *
   */
  public graduationRequestConsents = [];

  /**
   *
   * The status of the checkboxes
   *
   */
  public consents = [];

  /**
   * A list of user consent items related to the graduation
   */
  public userConsents = [];

  /**
   *
   * The name of the request. This message is completed by the application
   *
   */
  public requestName: string;

  /**
   *
   * A message that the student can send alongside their graduation request
   *
   */
  public specialRequest: string;

  /**
   * The graduation event
   */
  public selectedEvent: any;
  public selectedEventId: any;

  public isLoading = true;

  /**
   * The graduation form
   */
  public studentRequest = {
    specialRequest: '',
    participateInCeremony: false,
    requirementsChecked: false
  };

  public errorMessage: string;

  constructor(
    private _translateService: TranslateService,
    private _graduationService: GraduationService,
    private _errorService: ErrorService) {
    super();
  }


  ngOnInit() {
    if (this.graduationRequestStatusObservable$) {
      this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((step) => {
        this.fillData();
        if (step) {
          this.currentStep = step;
        }
      });
    } else {
      this.fillData();
    }
  }

  fillData() {
    if (this.currentStep && this.currentStep.data) {
      if (this.currentStep.data.request) {
        this.selectedEvent = this.currentStep.data.request.graduationEvent;
        this.submitted = true;
        this.requestName = this.currentStep.data.request.name;
        this.specialRequest = this.currentStep.data.request.description;

      } else {
        // this.selectedEvent = null;
        this.submitted = false;
      }
      this.userConsents = this.currentStep.data.userConsents;
    }

    this.graduationRequestConsents = [
      {
        id: 'participateInCeremony',
        translationKey: this._translateService.instant( 'UniversisGraduationModule.GraduationRequest.ParticipateInCeremony', {
          academicPeriod: this._translateService.instant(
            `UniversisGraduationModule.Semester.${this.currentStep.data.events[0].graduationPeriod.alternateName}Possessive`
          ),
        }),
        required: false
      },
      {
        id: 'requirementsChecked',
        translationKey: 'UniversisGraduationModule.GraduationRequest.RequirementsChecked',
        required: true
      }
    ];
    this.consents = new Array(this.graduationRequestConsents.length).fill(false);
  }

  ngOnDestroy(): void {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

  /**
   *
   * Updates the status of a checkbox and decides if the form can be submitted
   *
   * @param event The HTML event that triggered the status change
   * @param index The index of the checkbox to update it's status
   *
   */
  updateConsentsStatus(event: any, index: number): void {
    this.consents[index] = event.target.checked;

    this.canSubmit = this.graduationRequestConsents.reduce((aggregator, consentItem, i) => {
      if (consentItem.required) {
        return aggregator && this.consents[i];
      } else {
        return aggregator;
      }
    }, true);

    if (!this.selectedEvent) {
      this.canSubmit = false;
    }
  }

  toggleUserConsent(event: any, index: number): void {
    this.userConsents[index].val = event.target.checked ? 'y' : 'n';
    const request = this.currentStep.data && this.currentStep.data.request;
    if (request && request.id) {
      this._graduationService
        .setGraduationUserConsents(this.userConsents)
        .catch((err) => {
          console.error(err);
          this._errorService.showError(err, {
            continueLink: '.'
        });
      });
    }
  }

  /**
    * selected Event
   **/
  onEventChange(event: any) {
    const eventId = number(event.target.value);

    this.selectedEvent = this.currentStep.data.events.filter( e => {
      // tslint:disable-next-line:triple-equals
      return e.id == eventId;
    }).shift();

    this.requestName = this._translateService.instant('UniversisGraduationModule.GraduationRequest.DefaultRequestName', {
      academicPeriod: this._translateService.instant(
        `UniversisGraduationModule.Semester.${this.selectedEvent.graduationPeriod.alternateName}`
      ),
      academicYear: this.selectedEvent.graduationYear.alternateName
    });

    this.canSubmit = this.graduationRequestConsents.reduce((aggregator, consentItem, i) => {
      if (consentItem.required) {
        return aggregator && this.consents[i];
      } else {
        return aggregator;
      }
    }, true);

    if (this.canSubmit && this.selectedEvent) {
      this.canSubmit = true;
    }
  }

  /**
   * submits the form
   */
  async submit() {
    const form = {
      name: this.requestName,
      specialRequest: this.studentGraduationRequest.value['graduation-special-request'],
      participateInCeremony: this.studentGraduationRequest.value.participateInCeremony,
      requirementsChecked: this.studentGraduationRequest.value.requirementsChecked,
      userConsents: this.userConsents,
    };

    this.currentStepOutput.emit({
      form,
      graduationEvent: this.selectedEvent
    });

  }

}
