import { Component, OnInit, OnDestroy } from '@angular/core';
import { GraduationWizardTabComponent } from '../../../graduation-wizard-tab/graduation-wizard-tab.component';

@Component({
  selector: 'universis-graduation-ceremony',
  templateUrl: './graduation-ceremony.component.html'
})
export class GraduationCeremonyComponent extends GraduationWizardTabComponent implements OnInit, OnDestroy {

  constructor() {
    super();
  }

  /**
   * A list of certificates for the student.
   */
  public certificates: Array<any>;
  public selectedEvent: any;

  /**
   * A message for the student in case of failed graduation
   */
  public message: string;


  ngOnInit() {
    if (this.graduationRequestStatusObservable$) {
      this.graduationRequestStatusSubscription = this.graduationRequestStatusObservable$.subscribe((step) => {
        this.fillData();
        if (step) {
          this.currentStep = step;
        }
      });
    } else {
      this.fillData();
    }
  }

  fillData() {
    if (this.currentStep && this.currentStep.data) {
      this.certificates = this.currentStep.data.certificates;
      this.selectedEvent = this.currentStep.data.request ? this.currentStep.data.request.graduationEvent : null;
    } else {
      return;
    }
  }

  ngOnDestroy() {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

}
