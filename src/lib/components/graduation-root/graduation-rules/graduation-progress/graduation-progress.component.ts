import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { GraduationService } from '../../../../services/graduation-request/graduation.service';


@Component({
  selector: 'universis-graduation-progress',
  templateUrl: './graduation-progress.component.html',
  styleUrls: ['./graduation-progress.component.scss']
})
export class GraduationProgressComponent implements OnInit, OnChanges {
  public messageRes: void;
  public courseTypes: any;
  public studentSpecialty: any;
  public studentGuide: any;
  public isLoading = true;
  public graduationInfo: any;
  private fragmentSubscription: Subscription;
  @Input('student') student: any;
  @Output() requirementsCheck = new EventEmitter<any>();

  constructor(private _loadingService: LoadingService,
              private _activatedRoute: ActivatedRoute,
              private _graduationRequestService: GraduationService) {}

  async ngOnInit() {
    // Reload data when the graduation-rules modal is closed to update the component's view.
    // Graduation rules or the defined complex rule might have changed.
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        if (this.student) {
          this.ngOnChanges(<SimpleChanges>{
            student: <SimpleChange>{
              previousValue: this.student,
              currentValue: this.student
            }
          });
        }
      }
    });
  }

    ngOnChanges(changes: SimpleChanges): void {
    if (changes.student) {
      if (changes.student.currentValue == null) {
        this.student = null;
        return;
      }
      (async () => {
        try {
          this._loadingService.showLoading();
          this.student = changes.student.currentValue;
          const getGraduationRules = this._graduationRequestService.getGraduationRules(this.student);
          const getCourseTypes = this._graduationRequestService.getCourseTypes();
          const [graduationInfo, courseTypes] = await Promise.all([getGraduationRules, getCourseTypes]);
          // notify parent component of wether the student meets the graduation requirements
          if (graduationInfo && graduationInfo.finalResult) { this.requirementsCheck.emit(graduationInfo.finalResult.success) };
          this.courseTypes = courseTypes;
          this.graduationInfo = graduationInfo;
          this._loadingService.hideLoading();
          this.isLoading = false;
        } catch (err) {
          this._loadingService.hideLoading();
          this.isLoading = false;
          console.warn(err);
        }
      })().then(() => {
        //
      }).catch((err) => {
        console.warn(err);
      });
    }
  }

  fitsCircle(value) {
    if (typeof value === 'string' || typeof value === 'number') {
      return typeof value === 'number'
        ? value < 1000
        : value.length < 4;
    } else {
      return true;
    }
  }

}
