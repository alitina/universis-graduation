import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorService } from '@universis/common';
import {Tree, TreeSettings} from 'gijgo';
import * as esprima from 'esprima';

declare var $: any;

@Component({
  selector: 'universis-graduation-tree',
  templateUrl: './graduation-tree.component.html'
})
export class GraduationTreeComponent implements AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('tree') treeRef: ElementRef;
  @Input() rules: any;
  @Input() displayProgress: boolean;
  @Output() ruleExpressionChange = new EventEmitter<any>();
  @Output() back = new EventEmitter<any>();

  public tree: Tree;
  public operators = ['&&', '||', '!'];
  public lastId = 0;
  public expression = '';
  public expressionToPost = '';
  public isValidExp = true;
  public oneRuleExpression = false;
  public isLoading = true;
  public warningMessage: string = null;

  private expressionFormatter = {
    // Format the expression so that esprima is able to parse it.
    // Keep only rule ids and replace operator literals with the corresponding symbols
    toEsprima: (exp: string) => exp.replace(/\[\%(\d+)\]/g, '$1').replace(/AND|OR|NOT/gi, op => this.opsMapping.symbol(op)),

    // Enclose rule ids in [% ] and replace operator symbols with the corresponding literals
    toPost: (exp: string) => exp.replace(/\&\&|\|\||\!/g, op => this.opsMapping[op].literal).replace(/(\d+)/g, '[%$1]'),

    // translate operator symbols
    toDisplay: (exp: string) => exp.replace(/\&\&|\|\||\!/g, op => this.opsMapping[op].translation)
  };

  private opsMapping = {
    symbol(literal: string) {
      return Object.keys(this).find(key => this[key]['literal'] === literal.toUpperCase());
    },
    '&&': {
      // tslint:disable-next-line: max-line-length
      template: `<div class="operator-container"><div class="operator"><span>${this._translateService.instant('UniversisGraduationModule.LogicalOperators.&&')}</span></div></div>`,
      translation: this._translateService.instant('UniversisGraduationModule.LogicalOperators.&&'),
      literal: 'AND'
    },
    '||': {
      // tslint:disable-next-line: max-line-length
      template: `<div class="operator-container"><div class="operator"><span>${this._translateService.instant('UniversisGraduationModule.LogicalOperators.||')}</span></div></div>`,
      translation: this._translateService.instant('UniversisGraduationModule.LogicalOperators.||'),
      literal: 'OR'
    },
    '!': {
      // tslint:disable-next-line: max-line-length
      template: `<div class="operator-container"><div class="operator"><span>${this._translateService.instant('UniversisGraduationModule.LogicalOperators.!')}</span></div></div>`,
      translation: this._translateService.instant('UniversisGraduationModule.LogicalOperators.!'),
      literal: 'NOT'
    }
  };
  private dataSourceNode = {
    text: null,
    value: null,
    type: null,
    id: null,
    children: []
  };
  private configuration: TreeSettings = {
    primaryKey: 'id',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    icons: {
      expand: '<i class="fa fa-chevron-up mr-2"></i>',
      collapse: '<i class="fa fa-chevron-down mr-2"></i>'
    },
    dragAndDrop: false
  };

  constructor(
    private _errorService: ErrorService,
    private _translateService: TranslateService) { }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.rules) {
      if (changes.rules.currentValue == null) {
        return;
      }

      // destroy previous tree (if any) and load the new tree
      this.ngOnDestroy();
      this.rules = changes.rules.currentValue;
      this.loadData();
      this.ngAfterViewInit();
    }
  }

  loadData() {
    try {
      if (this.rules.length === 1) { this.oneRuleExpression = true; }
      const initialExpression = this.rules[0].ruleExpression || this.getDefaultExpression();
      this.configTreeDataSource(initialExpression);
      this.isLoading = false;
    } catch (error) {
      console.error(error);
      return this._errorService.navigateToError(error);
    }
  }

  /**
 * Calculates a default AND expression with the available rules.
 */
  getDefaultExpression(): string | '' {
    let ruleExpression = '';
    if (this.rules.length > 1) {
      ruleExpression += this.rules[0].id;
      for (const rule of this.rules.slice(1)) { ruleExpression += ` AND ${rule.id}`; }
    }
    return ruleExpression;
  }

  getInitialTreeData(exp: string) {
    exp = this.expressionFormatter.toEsprima(exp);
    const parsedExp = esprima.parseScript(exp);
    const treeData = JSON.parse(JSON.stringify(this.dataSourceNode));
    this.parse(parsedExp.body[0]['expression'], treeData);
    return treeData;
  }

  configTreeDataSource(exp: string) {
    if (exp) {
      const treeData = this.getInitialTreeData(exp);
      this.validateNodes(treeData);
      this.configuration.dataSource = [treeData];
      this.getExpression(treeData);
    } else {
      this.configuration.dataSource = [];
    }
  }

  validateNodes(node) {
    if (node.type === 'rule') {
      const ruleSatisfied = node.value.validationResult.success;
      node.evaluationResult = ruleSatisfied;
      return ruleSatisfied;
    }

    if (node.type === 'operator') {
      if (node.value === '!') {
        node.evaluationResult =  !this.validateNodes(node.children[0]);
        this.decorateOperator(node);
        return node.evaluationResult;
      }

      const operands = node.children.map(child => {
        return this.validateNodes(child);
      });

      if (node.value === '||') {
        node.evaluationResult = operands.reduce((a, b) => a || b);
        this.decorateOperator(node);
        return node.evaluationResult;
      }

      if (node.value === '&&') {
        node.evaluationResult = operands.reduce((a, b) => a && b);
        this.decorateOperator(node);
        return node.evaluationResult;
      }

    }
  }

  decorateOperator(node) {
    if (!this.displayProgress) { return; }
    const textColor = node.evaluationResult ? 'success' : 'danger';
    const faIcon = node.evaluationResult ? 'check' : 'times';
    const operator = this.opsMapping[node.value].translation;
    const template = `
        <div class="operator-container">
          <span class="fa-stack">
            <i class="fa fa-circle fa-stack-2x text-${textColor} font-md"></i>
            <i class="fa fa-${faIcon} fa-stack-1x fa-inverse"></i>
          </span>
          <div class="operator">
            <span>${operator}</span>
          </div>
        </div>`;

    node.text = template;
  }

  /**
  * Adds a unary NOT expression with the available rule to the tree.
  */
  setNotExpression() {
    const initialExpression = 'NOT ' + this.rules[0].id;
    const node = this.getInitialTreeData(initialExpression);
    this.tree.addNode(node, null, null);
    this.tree.expandAll();
  }

  /**
 * Destroys the tree and rebuilds it with the default AND expression
 */
  resetExpression() {
    this.ngOnDestroy();
    this.expression = '';
    this.expressionToPost = '';
    this.lastId = 0;
    const exp = this.getDefaultExpression();
    this.configTreeDataSource(exp);
    this.ngAfterViewInit();
  }

  getRuleTemplate(rule) {
    const validationResult = (rule.validationResult && rule.validationResult.data)
      // tslint:disable-next-line: max-line-length
      ? (typeof rule.validationResult.data.result === 'number' || rule.validationResult.data.result)  ? rule.validationResult.data.result : ''
      : '';
    const validationMessage = (rule.validationResult && rule.validationResult.message) ? rule.validationResult.message : '';
    const validationSuccess = (rule.validationResult && rule.validationResult.success) ? rule.validationResult.success : false;
    const textColor = validationSuccess ? 'success' : 'danger';

    const circleTemplate = !this.displayProgress
      ? `<span class="rule-circle text-secondary">
          <i class="fa fa-lock"></i>
        </span>`
      // tslint:disable-next-line: max-line-length
      : (typeof validationResult === 'number' && validationResult < 1000 || typeof validationResult === 'string' && validationResult.length < 4)
          ? `<div class="rule-circle text-${textColor}">
              <div class="font-lg">${validationResult}</div>
             </div>`
          : `<div class="rule-circle circle-text text-${textColor}">
               <div class="rule-tooltip">
                 <div class="font-lg text-truncate">${validationResult}</div>
                 <span class="rule-tooltiptext">${validationResult}</span>
               </div>
             </div>`;

    const messageTemplate = `<span class="text-secondary">${validationMessage}.</span>`;
    const template = `<div class="rule-circle-line">${circleTemplate + messageTemplate}</div>`;
    return template;
  }

  /**
  * Parses an esprima expression recursively and configures a data source compatible with the gijgo tree.
  * @param exp - A parsed esprima expression.
  * @param node - The tree node to be configured.
  */
  parse(exp, node) {
    // if the id of the node is not null, then the node has already been configured
    if (!node.id) {
      if (exp.type === 'Literal') {
        const currentRule = this.rules.find(rule => rule.id === Number(exp.value));
        node.text = this.getRuleTemplate(currentRule);
        node.value = currentRule;
        node.type = 'rule';
        node.id = ++this.lastId;
      }
      if (exp.operator) {
        node.text = this.opsMapping[exp.operator].template;
        node.value = exp.operator;
        node.type = 'operator';
        node.id = ++this.lastId;
      }
      if (exp.type === 'UnaryExpression') {
        // push a deep copy of an empty dataSourceNode
        const index = node.children.push(JSON.parse(JSON.stringify(this.dataSourceNode)));
        this.parse(exp.argument, node.children[index - 1]);
      }
    }

    if (exp.left && !(exp.left.operator && exp.operator === exp.left.operator)) {
      // push a deep copy of an empty dataSourceNode
      const index = node.children.push(JSON.parse(JSON.stringify(this.dataSourceNode)));
      this.parse(exp.left, node.children[index - 1]);
    } else if (exp.left) {
      this.parse(exp.left, node);
    }

    if (exp.right && !(exp.right.operator && exp.operator === exp.right.operator)) {
      // push a deep copy of an empty dataSourceNode
      const index = node.children.push(JSON.parse(JSON.stringify(this.dataSourceNode)));
      this.parse(exp.right, node.children[index - 1]);
    } else if (exp.right) {
      this.parse(exp.right, node);
    }
  }

  /**
  * Calculates the rule expressions that will be posted and displayed.
  * @param data - The tree's base node.
  */
  getExpression(data = null) {
    try {
      this.isValidExp = true;
      this.warningMessage = null;

      const treeData = data || this.tree.getAll()[0];
      let flatExp = this.flatten(treeData, '');
      if (flatExp && this.isValidExp) {
        // All expressions, except those that start with a NOT operator, are enclosed in '()'.
        // Remove redundant parentheses and then add whitespace between operators and rules.
        let shouldPost = false;
        flatExp = flatExp[0] === '(' ? flatExp.substring(1, flatExp.length - 1) : flatExp;
        flatExp = flatExp.replace(/\&\&|\|\||\!/g, op => {
          if (op !== '&&') { shouldPost = true; } // expressions that include only AND operators should not be posted
          return op === '!' ?  `${op} ` : ` ${op} `;
        });

        // A rule expression must include all the input rules in order to be valid.
        const rulesUsed = Array.from(new Set(flatExp.match(/\d+/g)));
        for (const rule of this.rules) {
          if (!rulesUsed.includes(String(rule.id))) {
            this.isValidExp = false;
            this.warningMessage = 'UnusedRules';
            break;
          }
        }

        this.expression = this.expressionFormatter.toDisplay(flatExp);
        this.expressionToPost = shouldPost ? this.expressionFormatter.toPost(flatExp) : '';
      }
      this.ruleExpressionChange.emit(this.isValidExp);
    } catch (error) {
      console.error(error);
      return this._errorService.navigateToError(error);
    }
  }

  flatten(node, flat: string): string | '' {
    if (!this.isValidExp) { return ''; }

    if (node.type === 'operator') {
      if (!this.validOpNode(node)) { return; }
      if (node.value === '!') { flat += node.value; } else { flat += '('; }
      if (node.children) {
        node.children.forEach((child, index) => {
          flat = this.flatten(child, flat);
          if (node.value !== '!' && index < (node.children.length - 1)) { flat += node.value; }
        });
      }
      if (node.value !== '!') { flat += ')'; }
    } else if (node.type === 'rule') {
      flat += node.value.id;
    }

    return flat;
  }

  validOpNode(op) {
    if (op.value === '!') {
      if (op.children && op.children.length === 1) { return true; }
      this.warningMessage = 'InvalidNotExp';
    }
    if (op.value !== '!') {
      if (op.children && op.children.length >= 2) { return true; }
      this.warningMessage = op.value === '&&' ? 'InvalidAndExp' : 'InvalidOrExp';
    }

    this.isValidExp = false;
    return false;
  }


  ngAfterViewInit() {
    if (this.treeRef) {
      try {
        this.tree = $(this.treeRef.nativeElement).tree(this.configuration);
        this.tree.expandAll();

        const tree = this.tree;
        const self = this;
        tree.on('nodeDrop', function (e, id, parentId, orderNumber) {
          const parent = tree.getDataById(parentId);
          if (parent.type === 'rule') {
            return false;
          }
          // can't use self.getExpression() to validate the resulting expression cause
          // the tree's data are updated after the event is fired. As a workaround,
          // we change te position of the node manually, so that the 'nodeDataBound' event is fired.
          const temp = tree.getDataById(String(id));
          const tempNode = tree.getNodeById(String(id));
          const parentNode = tree.getNodeById(String(parentId));
          tree.removeNode(tempNode);
          tree.addNode(temp, parentNode, null);

          // expand the parent node and its children
          tree.expand(parentNode, true);
          return false;

        });

        this.tree.on('select', function (e, node, id) {
          const nodeData = tree.getDataById(id);
          if (nodeData.type === 'operator') {
            $('#operator').prop('disabled', false);
            $('#operator').val(nodeData.value);
          } else {
            $('#operator').prop('disabled', true);
          }
        });

        this.tree.on('nodeDataBound', function () {
          self.getExpression();
        });

      } catch (error) {
        console.error(error);
        return this._errorService.navigateToError(error);
      }
    }
  }

  save() {
    // The select element is disabled when a rule is selected. Rule nodes shouldn't change, so return.
    if ($('#operator').prop('disabled')) { return; }

    // Get the selected node's id. If no node is selected return
    const id = this.tree.getSelections()[0];
    if (!id) { return; }

    const record = this.tree.getDataById(id);
    record.value = $('#operator').val();
    record.text = this.opsMapping[record.value].template;
    this.tree.updateNode(id, record);
  }

  remove() {
    // Get the selected node's id. If no node is selected return
    const id = this.tree.getSelections()[0];
    if (!id) { return; }

    const node = this.tree.getNodeById(String(id));
    if (!id || id === '1') { return; }
    this.tree.removeNode(node);
    this.getExpression();
  }

  /**
  * Appends an operator/rule to the selected node or to the root node depending on the selected node's type.
  * @param rule - A rule object.
  */
  append(rule = null) {
    const parent = this.getOpNode();
    const op = String($('#operator').val());
    const newNode = {
        text: rule
          ? `${rule.id} - ${rule.description || this._translateService.instant(`UniversisGraduationModule.${rule.refersTo}Rule.Title`)}`
          : this.opsMapping[op].template,
        id: ++this.lastId,
        type: rule ? 'rule' : 'operator',
        value: rule || op
      };
    this.tree.addNode(newNode, parent, null);
  }

  /**
   * Returns the selected node in case it's an operator node, otherwise it returns the root node.
   */
  getOpNode() {
    const id = this.tree.getSelections()[0];
    return (id && this.tree.getDataById(id).type === 'operator')
      ? this.tree.getNodeById(String(id))
      : this.tree.getNodeById('1');
  }

  saveAndHide() {
    this.back.emit();
  }

  ngOnDestroy() {
    if (this.tree) { this.tree.destroy(); }
  }

}
