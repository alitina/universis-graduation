/*
 * Public API Surface of graduation
 */


export { GraduationModule } from './lib/graduation.module';
export { GraduationRoutingModule, graduationRequestRoutes } from './lib/graduation.router';
export { GRADUATION_LOCALES } from './lib/i18n/index';

// components
export { GraduationRequestComponent } from './lib/components/graduation-root/graduation-apply/graduation-request/graduation-request.component';
export { GraduationRequestFormComponent } from './lib/components/graduation-root/graduation-apply/graduation-request/steps/graduation-request-form/graduation-request-form.component';
export { GraduationCeremonyComponent } from './lib/components/graduation-root/graduation-apply/graduation-request/steps/graduation-ceremony/graduation-ceremony.component';
export {
  GraduationRequirementsCheckComponent
} from './lib/components/graduation-root/graduation-apply/graduation-request/steps/graduation-requirements-check/graduation-requirements-check.component';
export {
  GraduationDocumentSubmissionComponent
} from './lib/components/graduation-root/graduation-apply/graduation-request/steps/graduation-document-submission/graduation-document-submission.component';
export { GraduationService } from './lib/services/graduation-request/graduation.service';
export { GraduationProgressComponent } from './lib/components/graduation-root/graduation-rules/graduation-progress/graduation-progress.component';
export { GraduationPrerequisitesComponent } from './lib/components/graduation-root/graduation-rules/graduation-prerequisites/graduation-prerequisites.component';
export {
  GraduationApplyComponent
} from './lib/components/graduation-root/graduation-apply/graduation-apply.component';
export {GraduationSharedModule} from './lib/graduation-shared.module';
